FF.<l2,l3,ll1,ll2,a,b,d> = PolynomialRing(QQ, 7)
R.<Y,X> = PolynomialRing(FF, 2, order='deglex')

def dlog_rho(prec):
  return X+Y*sum(sum((-1)^(j+1)*stirling_number1(i,i-j+1)/factorial(j)
                        *X^j*Y^(i-j) for j in (1..i)) for i in (1..prec-1))

from sage.rings.polynomial.polydict import ETuple

# truncating a bivariate series up to degree prec
def truncation(P, prec):
  P2 = R(0)
  denom = P.denominator()
  n  = R(P.numerator())
  for key in n.dict().keys():
    if key[0] + key[1] <= prec:
      P2 += n.dict()[key]*Y^key[0]*X^key[1]
  return P2/denom

# the differential operator Delta defined in Section 3 of the paper
def Delta(P, prec):
  return truncation(-Y*((X-Y)*P.derivative(X) + Y*P.derivative(Y)), prec)

# Computation of the bivariate polynomial Q_n (see Section 3 of the paper), with n = prec
def rho(prec):
  P0 = dlog_rho(prec)
  P1 = Delta(P0, prec)
  tmp = P1/Y
  if tmp != tmp.numerator():
    print("error, fn rho:", P1)
  P2 = P1-R(tmp.numerator())
  P3 = P2
  for i in (1..prec):
    P3  = -Delta(P3, prec)
    P2 += P3
  return truncation((1-Y)*(1+P0)+Y*P2, prec)

# truncated multiplication of two bivariate series
def mul(S1, S2, prec):
  return truncation(S1*S2, prec)

# truncated powering of a bivariate series
def powers(S, i, prec):
  if S((0,0)) != 0:
    print("error, fn powers: leading coeff is nonzero")
  lS = [S]
  for j in (1..i-1):
	  lS.append(mul(lS[j-1], S, prec))
  return lS

# inversion of a bivariate series with nonzero leading coefficient
def inv(S, prec):
  c = S((0,0))
  if c == 0:
    print("error, fn inv: leading coeff is zero")
  S  /= c
  lS  = powers(S-1, prec, prec)
  return 1/c*(1+truncation(sum((-1)^i*lS[i-1] for i in (1..prec)), prec))

# Computes the logarithm of a function in the class C (see associated paper)
# this function assumes that the constant coefficient of the series is 1
def log(S, prec):
  if S((0,0)) != 1:
    print("error, fn log: leading coeff is not 1")
  S -= 1
  lS = powers(S, prec, prec)
  return truncation(sum(1/i*(-1)^(i+1)*lS[i-1] for i in (1..prec)), prec)

# Computes the logarithm of a function in the class C (see associated paper)
# General case for series with nonzero constant coefficient. It uses the
# variable v as a placeholder for the log of the leading coefficient
def log2(S, prec, v): 
  LC = S((0,0))
  if LC == 0:
    print("error, fn log2: leading coeff is 0") 
  if LC == 1:
    return log(S, prec)
  print("info fn log2: LC is ", S((0,0)))
  S /= LC
  S -= 1
  lS = powers(S, prec, prec)
  return v + truncation(sum(1/i*(-1)^(i+1)*lS[i-1] for i in (1..prec)), prec)

# Evaluation of a bivariate series at two bivariate series with positive
# valuation (i.e. the constant coefficient must be zero).
def eval_biv(Q, S1, S2, prec):
  d = Q.total_degree()
  lS1 = [1] + powers(S1, d, prec)
  lS2 = [1] + powers(S2, d, prec)
  di = Q.dict()
  return sum(di[k]*mul(lS1[k[0]], lS2[k[1]], prec) for k in di.keys())

#### From here, the notations X and Y represent the functions 
####     Xtilde = sqrt(log(log(log N)))/log(log N)) 
####     Ytilde = 1/sqrt(log(log N)) 


# Computes an equality between bivariate series from the constraint in the
# optimization problem and series developement of o(1) in a, b, and d
def compute_eq_NFS(A, B, D, prec):
  D_inv = inv(R(D), prec)
  B_inv = inv(R(B), prec)
  U0 = mul(D_inv, B_inv, prec)
  U1 = mul(mul(R(A), R(D), prec)*2/3 + D_inv/3, B_inv, prec)
  V0 = 1-X^2+3*(l3/3-l2)*Y^2+3*Y^2*log2(U0, prec, ll1)
  V1 = 1-X^2+3*(4/3*l3-l2)*Y^2+3*Y^2*log2(U1, prec, ll2)
  V0_inv = truncation(3*Y^2*inv(V0, prec), prec + 2)
  V1_inv = truncation(3*Y^2*inv(V1, prec), prec + 2)
  W0 = truncation(-l3*Y^2+X^2+Y^2*log(V0, prec), prec + 2)
  W1 = truncation(-l3*Y^2+X^2+Y^2*log(V1, prec), prec + 2)
  tmp = mul(V0_inv, W0, prec + 2)/Y^2
  if tmp.numerator() != tmp:
    print("error loglog_u0_div_log_u0")
  loglog_u0_div_log_u0 = R(tmp.numerator())
  tmp = mul(V1_inv, W1, prec + 2)/Y^2
  if tmp.numerator() != tmp:
    print("error loglog_u1_div_log_u1")
  loglog_u1_div_log_u1 = R(tmp.numerator())
  log_p0_div_u0logu0 = eval_biv(rho(prec), V0_inv, loglog_u0_div_log_u0, prec)
  log_p1_div_u1logu1 = eval_biv(rho(prec), V1_inv, loglog_u1_div_log_u1, prec)
  log_p0 = mul(mul(U0, log_p0_div_u0logu0, prec), V0, prec)
  log_p1 = mul(mul(U1, log_p1_div_u1logu1, prec), V1, prec)
  return truncation(1/6*log_p0+1/2*log_p1-4/3*A+2/3*B, prec)

# Returns the coefficient of the smallest term (for the graded lex ordering) in
# a bivariate series.
def smallest_term_coeff(P):
  assert(P != 0)
  return P.coefficients()[P.number_of_terms() - 1]

def smallest_term(P, with_coeff = False):
  assert P != 0, 'polynomial should not be zero'
  m = P.monomials()[P.number_of_terms() - 1]
  if with_coeff:
      m = m * smallest_term_coeff(P)
  return m

# Returns the next monomial for the graded lex ordering.
def next_monomial(t):
  if t[0] > 0:
    return [t[0]-1, t[1]+1]
  else:
    return [t[0]+t[1]+1, 0]

# Guess the series A=B, D up to degree n for A and degree Floor(n/2) for D,
# without proving anything

def GuessTerms(n):
# Initialization using proved values up to degree 2 for A, 1 for D
  A = 1 + 4/3*X^2 + (- 2*l2 + 1/6*l3 - 2)*Y^2-4/9*X^4 + \
      (4/3*l2 - 1/9*l3 + 4)*X^2*Y^2 + \
      (- l2^2 + 1/6*l2*l3 - 6*l2 - 7/36*l3^2 + 1/2*l3 - 5)*Y^4
  D = 1 - 2/3*X^2 - (-2/3*l2 + 5/9*l3 - 2/3)*3/2*Y^2
  for deg in range(3, n+1):
    print('Computing degree:', deg)
    for j in range(0, deg+1):
      i = deg - j
      A2 = A + a*X^(2*i)*Y^(2*j)
      if i%2 == 0 and j%2 == 0:
        D2 = D + d*X^i*Y^j
      else:
        D2 = D
      prec = 2*i + 2*j
      eq_NFS = smallest_term_coeff(compute_eq_NFS(A2, A2, D2, prec))
      assert(eq_NFS.numerator() == eq_NFS)
      eq_NFS = eq_NFS.numerator()
      # assertions for ll1 and ll2
      assert eq_NFS.derivative(ll1) == 0, 'll1 should not appear in eq NFS'
      assert eq_NFS.derivative(ll2) == 0, 'll2 should not appear in eq NFS'
      # assert constant term
      assert eq_NFS.coefficient([0,0,0,0,0,0,0]) != 0, \
          'constant coeff should not be 0'
      # assertions for a
      assert eq_NFS.derivative(a) == -1, 'wrong coeff of a'
      # assertions for d
      if i%2 == 0 and j%2 == 0: 
        assert (eq_NFS.degree(d) == 2 and eq_NFS.coefficient({d:2}) == 1/3), \
          'wrong shape for d'
        val_d = -eq_NFS.derivative(d).coefficient({d:0})*3/2 
        D = D + val_d*X^i*Y^j
      else:
        assert eq_NFS.derivative(d) == 0
        val_d = 0
      val_a = eq_NFS((l2, l3, 0, 0, 0, 0, val_d))
      A = A + val_a*X^(2*i)*Y^(2*j)
      r = next_monomial([i,j])
      i = r[0]
      j = r[1]
  print('Series guessed')
  return [A, D]

# Given bivariate polynomials A, D, this function checks a sufficient
# condition (see Lemma 4.4) for the existence of functions satisfying the
# constraint of the optimization problem and such that their asymptotic
# behavior is described by A, B=A (at precision one less), D

def ProveExistence(A, D):
  n  = A.degree()
  B  = A
  A2 = A + a*X^(n+2)
  prec = n + 2
  constraint_asympt = compute_eq_NFS(A2, B, D, prec)
  eq_NFS = smallest_term_coeff(constraint_asympt)
  assert(eq_NFS.numerator() == eq_NFS)
  eq_NFS = eq_NFS.numerator()
  term  = smallest_term(constraint_asympt)
  constraint_asympt_rem = constraint_asympt - eq_NFS*term
  term2 = X^(n+3) if constraint_asympt_rem == 0 \
      else smallest_term(constraint_asympt_rem)
  print("constraint at precision", prec, ": (", eq_NFS, ')*', 
    term, ' = O(', term2,')')
  assert (eq_NFS.derivative(a) == -1)
  print('form of the equation is ok (cf. lemma 4.3)')  

# Given two bivariate polynomials A, D such that 2n = deg(A), deg(D) <= n, and
# which involve only even integers in the exponents, this functions checks
# sufficient conditions that generate a proof that the minimizers of the
# optimization problem are such that 
# a = ... * (A(X,Y) +o(Y^(2n)))
# b = ... * (A(X,Y) +o(Y^(2n-2)))
# d = ... * (D(X,Y) +o(Y^(n)))

def ProveMinimality(A, D):
  assert(A.degree()%2 == 0)
  n = ZZ(A.degree()/2)
  assert(D.degree() <= n)
  # initialization using Thm. 4.3
  assert(truncation(A,  4) == \
    1 + 4/3*X^2 + (- 2*l2 + 1/6*l3 - 2)*Y^2-4/9*X^4+\
    (4/3*l2 - 1/9*l3 + 4)*X^2*Y^2 \
    + (- l2^2 + 1/6*l2*l3 - 6*l2 - 7/36*l3^2 + 1/2*l3 - 5)*Y^4)
  assert(truncation(D, 2) == \
    1 - 2/3*X^2 - (-2/3*l2 + 5/9*l3 - 2/3)*3/2*Y^2)
  # check that all exponents are even
  for key in A.dict().keys():
    assert(key[0]%2 == 0 and key[1]%2 == 0)
  for key in D.dict().keys():
    assert(key[0]%2 == 0 and key[1]%2 == 0)
  for m in range(2, n):
    for i in range(0 if m != 2 else 2, m+1):
      # first, prove that the function o(1) is a bigOh(.)
      # Three cases depending if i==0, i==m, or 0<i<m
      print('proving that o() is a O() ')
      if i == m:
        A2 = truncation(A, 2*m)   + a*Y^(2*m)
        B2 = truncation(A, 2*m-2) + b*Y^(2*m-2)
        D2 = truncation(D, m)     + d*Y^m
        constraint_asympt = compute_eq_NFS(A2, B2, D2, 2*m+1)
        eq_NFS            = smallest_term_coeff(constraint_asympt)
        assert eq_NFS == eq_NFS.numerator(), 'there is a denominator'
        eq_NFS            = eq_NFS.numerator()
        term              = smallest_term(constraint_asympt)
        constraint_rem    = constraint_asympt - eq_NFS*term
        assert term == Y^(2*m), 'wrong lowest term'
        assert (m != 2 and eq_NFS == -a - 2*b + 1/3*d^2) or \
               (m == 2 and eq_NFS == -a - 2*b + 2/3*b^2 + 1/3*d^2),\
               'wrong shape of the constraint'
        if m%2 != 0:
          # all the remaining terms should be divisible by d
          assert constraint_rem != 0, 'remaining terms should not be zero'
          for tt in constraint_rem.coefficients():
            tmp = tt.numerator()
            assert tmp == tt, "there is a denominator"
            for monom in tmp.monomials():
              assert monom.derivative(d) != 0, 'not divisible by d'
          term_d = smallest_term(constraint_rem)
          coeff_d = smallest_term_coeff(constraint_rem)
          print('constraint: (', eq_NFS, ') * ', term, 
                '+', coeff_d, '*', term_d,
                '* (1+o(1)) = O(', X^(2*m+2),')')
          print('This is pattern (P2) -> ok')
          assert term_d == X^(m+1)*Y^m, 'wrong term for d'
        else: 
          assert constraint_rem == 0, 'remaining terms should be zero'
          print('constraint: (', eq_NFS, ') * ', term, 
                '= O(', X^(2*m+2), ')')
          print('This is pattern (P2) (with kappa=0) -> ok')
        # Computing the constants
        print('Proving the limits of the bounded functions')
        A2 = truncation(A, 2*m+2) + a*X^(2*m+2)
        B2 = truncation(A, 2*m)   + b*Y^(2*m-1)
        D2 = truncation(D, m+1)     + d*X^(m+1)
        constraint_asympt = compute_eq_NFS(A2, B2, D2, 2*m+2)
        eq_NFS            = smallest_term_coeff(constraint_asympt)
        assert eq_NFS == eq_NFS.numerator(), 'there is a denominator'
        eq_NFS            = eq_NFS.numerator()
        term              = smallest_term(constraint_asympt)
        constraint_rem    = constraint_asympt - eq_NFS*term
        eq_NFS2 = smallest_term_coeff(constraint_rem)
        term2             = smallest_term(constraint_rem)
        assert eq_NFS2 == eq_NFS2.numerator(), 'there is a denominator'
        eq_NFS2            = eq_NFS2.numerator()
        assert eq_NFS == -2*b, 'wrong shape for coefficients'
        assert eq_NFS2 == 1/3*d^2-a, 'wrong shape for coefficients'
        constraint_rem2 = constraint_rem-eq_NFS2*term2
        print(eq_NFS*term+eq_NFS2*term, "= o(", 
              X^(2*m+3) if constraint_rem2 == 0 else smallest_term(constraint_rem2), ') -> ok')
      elif i == 0:
        A2 = truncation(A, 2*m)   + a*X^(2*m)
        B2 = truncation(A, 2*m-2) + b*Y^(2*m-3)
        D2 = truncation(D, m)     + d*X^m
        constraint_asympt = compute_eq_NFS(A2, B2, D2, 2*m)
        eq_NFS1           = smallest_term_coeff(constraint_asympt)
        assert eq_NFS1 == eq_NFS1.numerator(), 'there is a denominator'
        eq_NFS1           = eq_NFS1.numerator()
        term1             = smallest_term(constraint_asympt)
        constraint_rem    = constraint_asympt - eq_NFS1*term1
        eq_NFS2 = smallest_term_coeff(constraint_rem)
        assert eq_NFS2 == eq_NFS2.numerator(), 'there is a denominator'
        eq_NFS2           = eq_NFS2.numerator()
        term2 = smallest_term(constraint_rem)
        constraint_rem2 = constraint_rem - eq_NFS2*term2
        termO = X^(2*m+1) if constraint_rem2 == 0 \
                          else smallest_term(constraint_rem2)
        assert termO >= X^(2*m-2)*Y^2, "bigO should be sufficiently large"
        print('constraint:', eq_NFS1*term1 + eq_NFS2*term2, '= O(', X^(2*m-2)*Y^2, ')')
        print('This is pattern (P3) -> ok')
        assert term1 == Y^(2*m-1), 'wrong lowest term'
        assert term2 == X^(2*m), 'wrong second lowest term'
        assert(eq_NFS1 == -2*b)
        assert(eq_NFS2 == -a + 1/3*d^2)
        # Computing the coefficients
        print('Proving the limits of the bounded functions.')
        A2 = truncation(A, 2*m)   + a*X^(2*m-2)*Y^2
        B2 = truncation(A, 2*m-2) + b*X^(2*m-2)
        D2 = truncation(D, m)     + d*X^(m-1)*Y
        constraint_asympt = compute_eq_NFS(A2, B2, D2, 2*m)
        eq_NFS            = smallest_term_coeff(constraint_asympt)
        assert eq_NFS == eq_NFS.numerator(), 'there is a denominator'
        eq_NFS            = eq_NFS.numerator()
        assert eq_NFS == 1/3*d^2-a-2*b, 'wrong shape for coefficients'
        print(eq_NFS, "-> ok")
      else:
        A2 = truncation(A, 2*m)   + a*X^(2*m-2*i)*Y^(2*i)
        B2 = truncation(A, 2*m-2) + b*X^(2*m-2*i)*Y^(2*i-2)
        D2 = truncation(D, m)     + d*X^(m-i)*Y^(i)
        constraint_asympt = compute_eq_NFS(A2, B2, D2, 2*m)
        eq_NFS            = smallest_term_coeff(constraint_asympt)
        assert eq_NFS == eq_NFS.numerator(), 'there is a denominator'
        eq_NFS            = eq_NFS.numerator()
        term              = smallest_term(constraint_asympt)
        constraint_rem    = constraint_asympt - eq_NFS*term
        termO  = X^(2*m+1) if constraint_rem == 0 \
                        else smallest_term(constraint_rem)
        assert termO >= Y^(2*i+2)*X^(2*m-2*i-2)
        assert eq_NFS == 1/3*d^2 - a - 2*b
        print('constraint: (', eq_NFS, ') *', term, '= O(',
              Y^(2*i+2)*X^(2*m-2*i-2), ')')
        print('This is pattern (P1) -> ok')
        # Computing the coefficients
        print('Proving the limits of the bounded functions.')
        A2 = truncation(A, 2*m)   + a*X^(2*m-2*i-2)*Y^(2*i+2)
        B2 = truncation(A, 2*m-2) + b*X^(2*m-2*i-2)*Y^(2*i)
        D2 = truncation(D, m)     + d*X^(m-i-1)*Y^(i+1)
        constraint_asympt = compute_eq_NFS(A2, B2, D2, 2*m)
        eq_NFS            = smallest_term_coeff(constraint_asympt)
        assert eq_NFS == eq_NFS.numerator(), 'there is a denominator'
        eq_NFS            = eq_NFS.numerator()
        assert eq_NFS == 1/3*d^2-a-2*b, 'wrong shape for coefficients'
        print(eq_NFS, "-> ok")

def ComputeProvenExpansion(n):
  print('\
Initial warning: In what follows, the symbols X and Y represent the functions\
\n\\sqrt{\\mathcal X} and \\sqrt{\\mathcal Y} in the companion paper')
  print('------------------------')
  print('First step: GuessTerms')
  print('------------------------')
  res = GuessTerms(n + 1)
  print('\
--------------------------------------------------------------------------------')
  print('\
Second step: prove existence of functions a, b, d satisfying the constraint, \
and\n\
having the asymptotic developement prescribed by the series guessed at first\n\
step')
  print('\
--------------------------------------------------------------------------------')
  ProveExistence(res[0], res[1])
  print('\
--------------------------------------------------------------------------------')
  print('\
Third step: Prove that the minimizers have the asymptotic development guessed \
at\nfirst step')
  print('\
--------------------------------------------------------------------------------')
  ProveMinimality(res[0], res[1])
  print('The series A,B,D associated to the minimizers a,b,d satisfy:')
  print('A =', res[0], '+o(',Y^(2*n+2), ')')
  print('B =', truncation(res[0], 2*n), '+o(',Y^(2*n), ')')
  print('D =', res[1], '+o(',Y^(n+1), ')')
