Refined Analysis of the Asymptotic Complexity of the Number Field Sieve
=======================================================================

This repository contains companion code for the paper *Refined Analysis
of the Asymptotic Complexity of the Number Field Sieve* by [Aude Le
Gluher](http://perso.eleves.ens-rennes.fr/people/Aude.Legluher/),
[Pierre-Jean Spaenlehauer](https://members.loria.fr/PJSpaenlehauer),
[Emmanuel Thomé](https://members.loria.fr/EThome)


The code here was written for [SageMath](https://sagemath.org/) version
9.0 or above, which depends on the Python3 interpreter. Behaviour with
older versions of SageMath might differ, especially in printing output.

tl;dr;
------

The classical heuristic complexity of NFS is 
```math
\exp((64/9)^{1/3}(\log N)^{1/3}(\log\log N)^{2/3}(1+o(1))).
```

The following commands compute the refined asymptotic expansion up to degree $`3`$ of the
function written `$(1+o(1))$` in the classical heuristic complexity of NFS:
```python
sage: load('rho.sage')
sage: ComputeProvenExpansion(3)
``` 
The number $`3`$ can be replaced by any integer >1. At the end of the output
printed by these commands, three asymptotic expansions --- called $`A`$, $`B`$,
$`D`$ --- are written. The asymptotic expansion of the function $`(1+o(1))`$ in the
classical heuristic complexity of NFS is obtained by substituting $`X`$ and
$`Y`$ by $`\sqrt{\log\log\log N/\log\log N}`$ and $`\sqrt{1/\log\log N}`$
respectively in the asymptotic expansion $`B`$.

The constants appearing as `l2` and `l3` in the coefficients of $`B`$ denote
the real values $`\log 2`$ and $`\log 3`$ respectively.

In this example for an expansion up to degree $`3`$, the formula for B is:
```python
B = (-4/3*l2^3 + 1/3*l2^2*l3 - 7/9*l2*l3^2 + 41/648*l3^3 - 14*l2^2 + 7/3*l2*l3 - 49/18*l3^2 - 32*l2 + 8/3*l3 - 85/3)*Y^6 + (8/3*l2^2 - 4/9*l2*l3 + 14/27*l3^2 + 56/3*l2 - 14/9*l3 + 64/3)*Y^4*X^2 + (-16/9*l2 + 4/27*l3 - 56/9)*Y^2*X^4 + 32/81*X^6 + (-l2^2 + 1/6*l2*l3 - 7/36*l3^2 - 6*l2 + 1/2*l3 - 5)*Y^4 + (4/3*l2 - 1/9*l3 + 4)*Y^2*X^2 + (-4/9)*X^4 + (-2*l2 + 1/6*l3 - 2)*Y^2 + 4/3*X^2 + 1 +o( Y^6 )
```

The function $`\varepsilon`$ in the proof of Lemma 4.4
------------------------------------------------------

We show here how to use [SageMath](https://sagemath.org/) to check our
claim on the existence of the function $`\varepsilon`$ in the proof of
Lemma 4.4

The code in [`rho.sage`](rho.sage) uses the notations `X` and `Y` to
denote what is actually $`\sqrt{\mathcal X}`$ and $`\sqrt{\mathcal Y}`$ in the paper.
As such, the expressions that are given in the proofs of Lemma 4.4 are
written as follows (here, `A` plays the role of
$`a/((8/9)\nu(\log\nu)^2)^{1/3}`$ in the paper (ditto for `B` and `D`), while
`a` plays the role of $`\tilde{a}`$):
```python
sage: B = (-l2^2 + 1/6*l2*l3 - 7/36*l3^2 - 6*l2 + 1/2*l3 - 5)*Y^4 + \
....:     (4/3*l2 - 1/9*l3 + 4)*Y^2*X^2 + \
....:     (-4/9)*X^4 + \
....:     (-2*l2 + 1/6*l3 - 2)*Y^2 + \
....:     4/3*X^2 + \
....:     1
sage: A = B + a * X^6
sage: D = (l2 - 5/6*l3 + 1)*Y^2 + (-2/3)*X^2 + 1
```

Our code is able to expand the equation that expresses the constraint on
$`a`$, $`b`$, and $`d`$. We obtain:
```python
sage: compute_eq_NFS(A, B, D, 6)
(-4/3*l2^3 + 1/3*l2^2*l3 - 7/9*l2*l3^2 + 41/648*l3^3 - 14*l2^2 + 7/3*l2*l3 - 49/18*l3^2 - 32*l2 + 8/3*l3 - 85/3)*Y^6 + (8/3*l2^2 - 4/9*l2*l3 + 14/27*l3^2 + 56/3*l2 - 14/9*l3 + 64/3)*Y^4*X^2 + (-16/9*l2 + 4/27*l3 - 56/9)*Y^2*X^4 + (-a + 32/81)*X^6
```
which matches the claim. Note that in this limited precision expansion
hides the dependency in $`\tilde{a}`$ (denoted as `a` in the code) of the
decaying terms. (We do observe a dependency in $`\tilde{a}`$ if we
proceed to precision 8.)

Direct computations in the proof of Proposition 4.3: Step 1
-----------------------------------------------------------

We use the same expressions as given in the paper.
```python
sage: A = 1 + a
sage: B = 1 + b
sage: D = 1 + d
sage: compute_eq_NFS(A, B, D, 2)
```
The last command computes a complicated expression, which we abbreviate
as:
```python
something*Y^2 + something*X^2 + (low_order_terms + 2/3*b^2 + 1/3*d^2 - a)/(1 + low_order_terms)
```
which corresponds to the claimed identity. The proof established that the next
step is to scale the terms `a`, `b`, and `d` to be respectively multiples of
`X^2`, `X`, and `X`.  When we do this, we obtain:
```python
sage: A = 1 + a*X^2
sage: B = 1 + b*X
sage: D = 1 + d*X
sage: compute_eq_NFS(A, B, D, 2)
(-2*l2 + 1/6*l3 - 2)*Y^2 + (2/3*b^2 + 1/3*d^2 - a + 4/3)*X^2
```
Here, `a`, `b`, and `d` stand for the asymptotically bounded functions $`\bar
a, \bar b, \bar d`$ in the proof.  This equality gives $`\bar a=4/3+o(1)`$,
$`\bar b=o(1)`$, $`\bar d=o(1)`$.

Direct computations in the proof of Proposition 4.3: Step 2
-----------------------------------------------------------

We refine further, and follow the same strategy as above. We first let
`a`, `b`, and `d` stand for the low-order functions $`\tilde{a}`$,
$`\tilde{b}`$, $`\tilde{d}`$, and rewrite the constraint:
```python
sage: A = 1 + 4/3 * X^2 + a*X^2
sage: B = 1 + b*X
sage: D = 1 + d*X
sage: compute_eq_NFS(A, B, D, 4)
something * Y^2 + (2/3*b^2 + 1/3*d^2 - a)*X^2
```
which is as written in the proof.
The next step is to determine the asymptotic behavior of $`\bar a`$, $`\bar b`$,
$`\bar d`$.
```python
sage: A = 1 + 4/3 * X^2 + a*Y^2
sage: B = 1 + b*Y
sage: D = 1 + d*Y
sage: compute_eq_NFS(A, B, D, 4)
low_order_terms + (2/3*b^2 + 1/3*d^2 - 2*l2 + 1/6*l3 - a - 2)*Y^2
```
which is sufficient to obtain $`\bar a=- 2\log 2 + \log3/6-2 + o(1)`$, $`\bar b
= o(1)`$, $`\bar d = o(1)`$.

Direct computations in the proof of Proposition 4.3: Step 3
-----------------------------------------------------------

As we refine further, more terms of the same total degree participate in
the expression obtained by substituting our limited precision expansion
in the expression of the constraint. The next refinement step is as
follows:
```python
sage: A = 1 + 4/3 * X^2 + (-2*l2 + 1/6*l3 - 2)*Y^2 + a * Y^2
sage: B = 1 + b*Y
sage: D = 1 + d*Y
sage: 3*compute_eq_NFS(A, B, D, 3)
((8*l2 - 2/3*l3 + 2 + lower_order_terms)*b - (2*l2 + 5/3*l3 - 2 + lower_order_terms)*d)*Y^3 + (-16/3*b + 4/3*d)*Y*X^2 + (2*b^2 + d^2 - 3*a)*Y^2
```
where the lower order terms exploit the fact that at this point, our
notations are such that $`\tilde{a}=o(1)`$, $`\tilde{b}=o(1)`$, and
$`\tilde{d}=o(1)`$.

We can verify the correctness of the rewriting as a quadratic expression:
```python
sage: smallest_term((d*Y+2/3*X^2+(-l2+5*l3/6-1+a/2-d^2/2)*Y^2)^2+2*(b*Y-4/3*X^2+
....: (2*l2-1/6*l3+1/2-a/4-b^2/2-d^2/4)*Y^2)^2-3*a*Y^2-3*compute_eq_NFS(A, B, D,
....:  4))
X^4
```
The equation on $`\bar a`$, $`\bar b`$, $`\bar d`$ is obtained as follows.
```python
sage: A = 1 + 4/3 * X^2 + (-2*l2 + 1/6*l3 - 2)*Y^2 + a * X^4
sage: B = 1 + b*X^2
sage: D = 1 + d*X^2
sage: EQ = compute_eq_NFS(A, B, D, 4)
sage: smallest_term(EQ)
X^4
sage: smallest_term_coeff(EQ)
(2/3*b^2 + 1/3*d^2 - a - 16/9*b + 4/9*d + 8/9)
sage: smallest_term_coeff(EQ) == (2/3*(b-4/3)^2+1/3*(d+2/3)^2-(a+4/9))
True
```

Direct computations in the proof of Proposition 4.3: Step 4
-----------------------------------------------------------

We proceed as before.

```python
sage: A = 1 + 4/3 * X^2 + (-2*l2 + 1/6*l3 - 2)*Y^2 + -4/9*X^4 + a * X^4
sage: B = 1 + 4/3*X^2 + b*X^2
sage: D = 1 - 2/3*X^2 + d*X^2
sage: EQ = compute_eq_NFS(A, B, D, 4)
sage: truncation(EQ,4).monomials()
[Y^4, Y^2*X^2, X^4]
sage: smallest_term_coeff(EQ)
2/3*b^2 + 1/3*d^2 - a
```

Then we get $`\bar a`$ as follows:

```python
sage: A = 1 + 4/3 * X^2 + (-2*l2 + 1/6*l3 - 2)*Y^2 + -4/9*X^4 + a * X^2*Y^2
sage: B = 1 + 4/3*X^2 + b*X*Y
sage: D = 1 - 2/3*X^2 + d*X*Y
sage: EQ = compute_eq_NFS(A, B, D, 4)
sage: truncation(EQ,4).monomials()
[Y^4, Y^2*X^2, X^4]
sage: smallest_term_coeff(EQ)
2/3*b^2 + 1/3*d^2 + 4/3*l2 - 1/9*l3 - a + 4
```

Direct computations in the proof of Proposition 4.3: Step 5
-----------------------------------------------------------

The following computation shows that $`\tilde a=O(\mathcal X^{-1}\mathcal Y)`$
and $`\tilde b, \tilde d=O(\sqrt{\mathcal X^{-1}\mathcal Y})`$:
```python
sage: A = 1 + 4/3 * X^2 + (-2*l2 + 1/6*l3 - 2)*Y^2 + -4/9*X^4 + (4*l2/3 - l3/9 + 4)*X^2*Y^2 + a*X^2*Y^2
sage: B = 1 + 4/3*X^2 + b*X*Y
sage: D = 1 - 2/3*X^2 + d*X*Y
sage: EQ = compute_eq_NFS(A, B, D, 4)
sage: smallest_term_coeff(EQ)*smallest_term(EQ)
(2/3*b^2 + 1/3*d^2 - a)*Y^2*X^2
```

To compute the limits associated to $`\tilde b`$ and $`\tilde d`$, we proceed as follows
```python
sage: A = 1 + 4/3 * X^2 + (-2*l2 + 1/6*l3 - 2)*Y^2 + -4/9*X^4 + (4*l2/3 - l3/9 + 4)*X^2*Y^2 + a*Y^4
sage: B = 1 + 4/3*X^2 + b*Y^2
sage: D = 1 - 2/3*X^2 + d*Y^2
sage: EQ = compute_eq_NFS(A, B, D, 4)
sage: smallest_term_coeff(EQ) == 2/3*(b+2*l2-l3/6+1/2)^2-3/2 + 1/3*(d+5/6*l3-l2-1)^2-(a-(-l2^2 + 1/6*l2*l3 - 7/36*l3^2 - 6*l2 + 1/2*l3 - 5))
True
```

Algorithm GuessTerms
--------------------

Algorithm GuessTerms can be used as follows. Its input n is an integer greater 
than 1. It returns a list of two bivariate polynomials which we shall call $`A`$
and $`D`$. The degree of $`A`$ is $`2n+2`$ and the degree of $`D`$ is $`n`$ if
$`n`$ is even, else the degree is $`n+1`$. All the monomials which appear in 
these polynomials are squares.These polynomials are candidates for being the 
asymptotic expansions of the minimizers of Problem 2.2 in the following sense:
The goal of the other functions (ProveExistence and ProveMinimality) of the code
will be to prove that

$`a = (8/9)^{1/3}\nu^{1/3}(\log\nu)^{2/3}(A(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^{n+1}))`$

$`b = (8/9)^{1/3}\nu^{1/3}(\log\nu)^{2/3}(A(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^n))`$

$`d = (3\nu/\log\nu)^{1/3}(D(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^{(n+1)/2}))`$


Algorithm ProveExistence
------------------------

The input of ProveExistence is two bivariate polynomials $`A`$ and $`D`$. Let 
$`n`$ be the degree of $`A`$. The aim of this function is to check sufficient 
conditions which ensure the existence of functions $`a`$, $`b`$ and $`d`$ which
satisfy the equality in Problem 2.2 and which are such that 

$`a = (8/9)^{1/3}\nu^{1/3}(\log\nu)^{2/3}(A(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^{n+1}))`$

$`b = (8/9)^{1/3}\nu^{1/3}(\log\nu)^{2/3}(A(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^{n+1}))`$

$`d = (3\nu/\log\nu)^{1/3}(D(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^{(n+1)/2}))`$

This is done by mimicking the proof of Lemma 4.4. This function does not return
anything: It just checks some assertions. If none of these assertions break, 
then this ensures the existence of such functions.

Algorithm ProveMinimality
-------------------------

The input of ProveMinimality is also two bivariate polynomials $`A`$ and $`D`$.
The aim of ProveMinimality is to prove that these polynomials are such that the
asymptotic expansions of the minimizers $`a`$, $`b`$ and $`d`$ of Problem 2.2
are

$`a = (8/9)^{1/3}\nu^{1/3}(\log\nu)^{2/3}(A(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^{n+1}))`$

$`b = (8/9)^{1/3}\nu^{1/3}(\log\nu)^{2/3}(A(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^n))`$

$`d = (3\nu/\log\nu)^{1/3}(D(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^{(n+1)/2}))`$

This function assumes that the function ProveExistence(A, D) has already been 
run and that no assertion has failed. This provides an upper bound on the 
minimum of the objective function of Problem 2.2.

This function does not return anything: its role is to check assertions which 
ensure that the minimizers should have these asymptotic expansions. See Section
4.3 of the paper for more details.

More coefficients of the expansion
----------------------------------

Beyond the coefficients that are reproduced in Section 5 of the paper, it is
possible to use our software to obtain more terms by using the function called
ComputeProvenExpansion. This function takes an integer $`n`$ as input and it
calls successively the three Algorithms GuessTerms, ProveExistence and
ProveMinimality in order to return (if no assertion fails in the process) two
bivariate polynomials $`A`$, $`D`$ such that the minimizers $`a`$, $`b`$, $`d`$
satisfy

$`a = (8/9)^{1/3}\nu^{1/3}(\log\nu)^{2/3}(A(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^{n+1})`$

$`b = (8/9)^{1/3}\nu^{1/3}(\log\nu)^{2/3}(A(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^n)`$

$`d = (3\nu/\log\nu)^{1/3}(D(\sqrt{\mathcal X}, \sqrt{\mathcal Y})+o(\mathcal Y^{(n+1)/2})`$
